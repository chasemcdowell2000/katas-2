//Function 1
function add(x, y) {
    return x + y;
}
console.log(add(2, 4));

//Function 2
function multiply(Num1, Num2) {
    let result2 = 0;
    for (i = 0; i < Num2; i++) {
        result2 += add(Num1, 0)
    }
    return result2;
}
 console.log(multiply(6, 8));

//Function 3
function power(x, n) {
    let total = 1;
    for (let i = 0; i < n; i++) {
        total = multiply(total, x);
    }
    return total;
}
console.log(power(3, 4));
//Function 4
function factorial(n) {
    let j = 1;
    for (i = 1; i <= n; i++) {
        j = multiply(j, i);
    }
    return j;
}
 console.log(factorial(4));

//Function 5
function fibonacci(y) {
    let x = [0, 1];
    for (let n = 2; n < y + 1; n += 1) {
        x.push(add(x[n - 2], x[n - 1]));
    }
    return x[y - 1];
}
console.log(fibonacci(8));
